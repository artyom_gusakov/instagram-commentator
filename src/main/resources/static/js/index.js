/*Обработка нажатия Enter в поисковой строке*/
$(document).on("keyup", "#search-input", function (e) {
    if (e.keyCode == 13) {
        searchUser();
    }
});

/*Обработка наведения курсора на комментарий*/
$(document).on("mouseenter", ".comment-element", function (e) {
    var commentId = $(this).attr("comment-id");
    //Отобразить кнопку удаления
    $(".comment-delete[comment-id=" + commentId + "]").css("display", "flex");
});
$(document).on("mouseleave", ".comment-element", function (e) {
    var commentId = $(this).attr("comment-id");
    //Отобразить кнопку удаления
    $(".comment-delete[comment-id=" + commentId + "]").css("display", "none");
});


/*Поиск пользователя по логину*/
function searchUser() {
    var userName = $(".user-name-search-input").val();
    console.log("search button submit user name = ", userName);
    $(".user-card").empty();
    $(".user-posts").empty();

    $.ajax({
        url: "/accounts/user-info?user-name=" + userName,
        beforeSend: function () {
            showPreloader();
        },
        success: function (data) {
            hidePreloader();
            $(".user-card").html(data);
        },
        error: function (jqXHR) {
            hidePreloader();
            console.log(jqXHR)
            if (jqXHR.status === 404) {
                $(".user-card").html("<h3>Пользователь не найден</h3>");
            }
            if (jqXHR.status === 500) {
                $(".user-card").html("<h3>Произошла непредвиденная ошибка ;(</h3>");
            }
        }
    });
}

/*Получение постов пользователя*/
function getUserRecentPost() {
    var userId = $("#user-card-account-id").val();
    console.log("search recent posts of user = ", userId);
    $(".user-posts").empty();
    $.ajax({
        url: "/accounts/user-recent-post?user-id=" + userId,
        beforeSend: function () {
            showPreloader();
        },
        success: function (data) {
            hidePreloader();
            $(".user-posts").html(data);
        },
        error: function (jqXHR) {
            hidePreloader();
            console.log(jqXHR)
            if (jqXHR.status === 404) {
                $(".user-posts").html("<h3>Пока ничего нет</h3>");
            }
            if (jqXHR.status === 500) {
                $(".user-posts").html("<h3>Произошла непредвиденная ошибка ;(</h3>");
            }
        }
    });
}

/*Показ детальной информации о посте*/
function showPostDetail(context) {

    var postId = $(context).attr("post-id");
    var postNormalResolutionUrl = $(context).attr("post-normal-resolution");
    var postCaption = $(context).attr("post-caption") ? $(context).attr("post-caption") : " ";
    console.log("postId = ", postId, "postNormalResolutionUrl = ", postNormalResolutionUrl, "postCaption = ", postCaption);

    $(".modal-title").text(postCaption);
    $(".modal-post-image").html("<img class='img-thumbnail img-responsive' src='" + postNormalResolutionUrl + "' width='300px' height='300px' />");

    $.ajax({
        url: "/media/" + postId + "/comments",
        beforeSend: function () {
            showPreloader();
        },
        success: function (data) {
            hidePreloader();
            $(".comment", $(".modal-post-comments")).html(data);
        },
        error: function (jqXHR) {
            hidePreloader();
            console.log(jqXHR)
            if (jqXHR.status === 404) {
                $(".comment", $(".modal-post-comments")).html("<div class='col-md-12 col-lg-12'><div class='comments-wrapper'><h4 comment-not-found>Нет комментариев</h4></div></div>");
            }
            if (jqXHR.status === 500) {
                $(".comment", $(".modal-post-comments")).html("<div class='col-md-12 col-lg-12'><div class='comments-wrapper'><h4 internal-server-error>Произошла непредвиденная ошибка ;(</h4></div>></div>");
            }
        }
    });
    $(".add-comment-submit").attr("post-id", postId);
    $("#post-detail-modal").modal('show');
}

function addComment() {
    var postId = $(".add-comment-submit").attr("post-id");
    var value = $('.add-comment-input').val();
    console.log("add comment to post with id = ", postId, " and comment content is ", value);
    $.ajax({
        url: "/media/" + postId + "/comments",
        method: "POST",
        dataType: "json",
        data: {text: value},
        beforeSend: function () {
            showPreloader();
        },
        success: function (data) {
            hidePreloader();
            console.log("Success add. Response is: ", data);
            var html = "<div class='comment-element' comment-id='" + data.id + "' post-id='" + postId + "'>";
            html += "<div class='comment-author'>" + data.from.username + "</div>";
            html += "<div class='comment-text'>" + data.text + "</div>";
            html += "<div class='comment-delete' comment-id='" + data.id + "' post-id='" + postId + "' onClick='deleteComment(this)'><div>Удалить</div></div>";
            html += "</div>";

            $("h4[comment-not-found]").remove();
            $("h4[internal-server-error]").remove();

            $(".comments-wrapper").append($(html));
            $('.add-comment-input').val("");
        },
        error: function (jqXHR) {
            hidePreloader();
            console.log("ERROR", jqXHR);
            if (jqXHR.status === 500) {
                alert("Произошла непредвиденная ошибка");
            }
        }
    })

}

function clearComment() {
    $(".add-comment-input").val("");

}

/*Удаление комментария*/
function deleteComment(context) {
    var commentId = $(context).attr("comment-id");
    var postId = $(context).attr("post-id");
    console.log("delete comment with id = ", commentId, " from post with id = ", postId);
    $.ajax({
        url: "/media/" + postId + "/comments/" + commentId,
        method: "DELETE",
        dataType: "json",
        beforeSend: function () {
            showPreloader();
        },
        success: function (data) {
            hidePreloader();
            if (data.status == 200) {
                $(".comment-element[comment-id=" + commentId + "]").remove();
            }
            else {
                $(".comment", $(".modal-post-comments")).html("<div class='col-md-12 col-lg-12'><div class='comments-wrapper'><h4 comment-not-found>Произошла непредвиденная ошибка ;(</h4></div></div>");
            }
        },
        error: function (jqXHR) {
            hidePreloader();
            console.log(jqXHR)
            if (jqXHR.status === 404) {
                $(".comment", $(".modal-post-comments")).html("<div class='col-md-12 col-lg-12'><div class='comments-wrapper'><h4 comment-not-found>Нет комментариев</h4></div></div>");
            }
            if (jqXHR.status === 500) {
                $(".comment", $(".modal-post-comments")).html("<div class='col-md-12 col-lg-12'><div class='comments-wrapper'><h4>Произошла непредвиденная ошибка ;(</h4></div></div>");
            }
        }
    });
}