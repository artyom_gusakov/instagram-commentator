package ru.gusakov.instagramcommentator.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import ru.gusakov.instagramcommentator.dto.*;

import java.io.IOException;

@Component
@Log4j
public class InstagramApiServiceImpl implements InstagramApiService {

    @Value("${instagram.access_token}")
    private String instagramAccessToken;

    private final String BASE_URL = "https://api.instagram.com/v1/";

    @Autowired
    private OkHttpClient okHttpClient;

    @Autowired
    private ObjectMapper jacksonObjectMapper;

    @Override
    public InstagramUserSearchResult findByUserName(String userName, int count) throws IOException {
        String requestUrl = BASE_URL + "users/search?q=" + userName + "&access_token=" + instagramAccessToken + "&count=" + count;
        log.info("User search request url: " + requestUrl);
        String response = runGet(requestUrl);
        InstagramUserSearchResult result = jacksonObjectMapper.readValue(response, InstagramUserSearchResult.class);
        return result;
    }

    @Override
    public InstagramRecentPostResult findUserRecentPost(String userId, int count) throws IOException {
        String requestUrl = BASE_URL + "users/" + userId + "/media/recent/?access_token=" + instagramAccessToken + "&count=" + count;
        log.info("Recent post request url: " + requestUrl);
        String response = runGet(requestUrl);
        InstagramRecentPostResult result = jacksonObjectMapper.readValue(response, InstagramRecentPostResult.class);
        return result;
    }

    @Override
    public InstagramCommentList findMediaComments(String mediaId) throws IOException {
        String requestUrl = BASE_URL + "media/" + mediaId + "/comments?access_token=" + instagramAccessToken;
        log.info("Post comments request url: " + requestUrl);
        String response = runGet(requestUrl);
        InstagramCommentList result = jacksonObjectMapper.readValue(response, InstagramCommentList.class);
        return result;
    }

    @Override
    public InstagramCommentChangeResult deleteMediaComment(String mediaId, String commentId) throws IOException {
        String requestUrl = BASE_URL + "media/" + mediaId + "/comments/" + commentId + "?access_token=" + instagramAccessToken;
        log.info("Delete comment request url: " + requestUrl);
        String response = runDelete(requestUrl);
        InstagramCommentChangeResult result = jacksonObjectMapper.readValue(response, InstagramCommentChangeResult.class);
        return result;
    }

    @Override
    public InstagramAddCommentResult addMediaComment(String mediaId, String text) throws IOException {
        String requestUrl = BASE_URL + "media/" + mediaId + "/comments/?access_token=" + instagramAccessToken;
        log.info("Delete comment request url: " + requestUrl);
        RequestBody body = new FormBody.Builder().add("text", text).build();
        String response = runPost(requestUrl, body);
        InstagramAddCommentResult result = jacksonObjectMapper.readValue(response, InstagramAddCommentResult.class);
        return result;
    }

    private String runPost(String url, RequestBody body) throws IOException {
        Request request = new Request.Builder().url(url).post(body).build();
        Response response = okHttpClient.newCall(request).execute();
        return response.body().string();
    }

    private String runGet(String url) throws IOException {
        Request request = new Request.Builder().url(url).build();
        Response response = okHttpClient.newCall(request).execute();
        return response.body().string();
    }


    private String runDelete(String url) throws IOException {
        Request request = new Request.Builder().url(url).delete().build();
        Response response = okHttpClient.newCall(request).execute();
        return response.body().string();
    }


}
