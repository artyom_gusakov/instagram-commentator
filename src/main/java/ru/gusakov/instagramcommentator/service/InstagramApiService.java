package ru.gusakov.instagramcommentator.service;

import java.io.IOException;

/**
 * Обертка для Instagram Api
 */

public interface InstagramApiService {
    /**
     * Get a list of users matching the query.
     *
     * @param userName - логин пользователя
     * @param count    - ограничение результата
     */
    <T> T findByUserName(String userName, int count) throws IOException;

    /**
     * Get the most recent media published by a user.
     *
     * @param userId - идентификатор пользователя
     * @param count  - количество постов
     */
    <T> T findUserRecentPost(String userId, int count) throws IOException;

    /**
     * Get a list of recent comments on a media object.
     */
    <T> T findMediaComments(String mediaId) throws IOException;

    /**
     * Remove a comment either on the authenticated user's media object or authored by the authenticated user.
     *
     * @param mediaId   - идентификатор поста
     * @param commentId - идентификатор комментария
     */

    <T> T deleteMediaComment(String mediaId, String commentId) throws IOException;

    /**Create a comment on a media object
     *@param mediaId   - идентификатор поста
     *@param text - текст комментария
     */
    <T> T addMediaComment(String mediaId, String text) throws IOException;
}
