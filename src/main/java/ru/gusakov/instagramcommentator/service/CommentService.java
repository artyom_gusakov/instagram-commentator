package ru.gusakov.instagramcommentator.service;


import ru.gusakov.instagramcommentator.model.entities.CommentEntity;

import java.util.ArrayList;
import java.util.List;

public interface CommentService {
    CommentEntity findById(String id);

    ArrayList<CommentEntity> findAll();

    CommentEntity save(CommentEntity entity);

    ArrayList<CommentEntity> save(List<CommentEntity> entityList);

    void delete(String id);
}
