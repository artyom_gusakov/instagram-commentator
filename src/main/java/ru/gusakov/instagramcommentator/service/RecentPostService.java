package ru.gusakov.instagramcommentator.service;


import ru.gusakov.instagramcommentator.model.entities.RecentPostEntity;

import java.util.ArrayList;
import java.util.List;

public interface RecentPostService {
    RecentPostEntity findById(String id);
    RecentPostEntity save(RecentPostEntity entity);
    ArrayList<RecentPostEntity> save(List<RecentPostEntity> entityList);
}
