package ru.gusakov.instagramcommentator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.gusakov.instagramcommentator.model.entities.RecentPostEntity;
import ru.gusakov.instagramcommentator.model.repositories.RecentPostRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class RecentPostServiceImpl implements RecentPostService {

    @Autowired
    private RecentPostRepository recentPostRepository;

    @Override
    public RecentPostEntity findById(String id) {
        return recentPostRepository.findOne(id);
    }

    @Override
    public RecentPostEntity save(RecentPostEntity entity) {
        return recentPostRepository.save(entity);
    }

    @Override
    public ArrayList<RecentPostEntity> save(List<RecentPostEntity> entityList) {
        return (ArrayList<RecentPostEntity>) recentPostRepository.save(entityList);
    }
}
