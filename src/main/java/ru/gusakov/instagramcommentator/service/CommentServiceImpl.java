package ru.gusakov.instagramcommentator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.gusakov.instagramcommentator.model.entities.CommentEntity;
import ru.gusakov.instagramcommentator.model.repositories.CommentRepository;

import java.util.ArrayList;
import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepository commentRepository;

    @Override
    public CommentEntity findById(String id) {
        return commentRepository.findOne(id);
    }

    @Override
    public ArrayList<CommentEntity> findAll() {
        return (ArrayList<CommentEntity>) commentRepository.findAll();
    }

    @Override
    public CommentEntity save(CommentEntity entity) {
        return commentRepository.save(entity);
    }

    @Override
    public ArrayList<CommentEntity> save(List<CommentEntity> entityList) {
        return (ArrayList<CommentEntity>) commentRepository.save(entityList);
    }

    @Override
    public void delete(String id) {
        commentRepository.delete(id);
    }
}
