package ru.gusakov.instagramcommentator.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.gusakov.instagramcommentator.model.entities.AccountEntity;
import ru.gusakov.instagramcommentator.model.repositories.AccountRepository;

import java.util.List;

@Service
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountRepository accountRepository;

    public List<AccountEntity> findAll() {
        return (List<AccountEntity>) accountRepository.findAll();
    }

    @Override
    public AccountEntity findOne(String id) {
        return accountRepository.findOne(id);
    }

    @Override
    public AccountEntity save(AccountEntity entity) {
        return accountRepository.save(entity);
    }

    @Override
    public List<AccountEntity> findByUserNameStartingWith(String prefix) {
        return accountRepository.findByUserNameStartingWith(prefix);
    }

    @Override
    public AccountEntity findByUserName(String userName) {
        return accountRepository.findByUserName(userName);
    }
}
