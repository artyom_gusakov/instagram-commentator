package ru.gusakov.instagramcommentator.service;

import ru.gusakov.instagramcommentator.model.entities.AccountEntity;

import java.util.List;

public interface AccountService {
    List<AccountEntity> findAll();
    AccountEntity findOne(String id);
    AccountEntity save(AccountEntity entity);
    List<AccountEntity> findByUserNameStartingWith(String startWith);
    AccountEntity findByUserName(String userName);
}
