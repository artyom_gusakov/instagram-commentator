package ru.gusakov.instagramcommentator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.sql.Timestamp;

@Data
public class InstagramComment {
    @JsonProperty("id")
    private String id;

    @JsonProperty("created_time")
    private Timestamp createdTime;

    @JsonProperty("text")
    private String text;

    @JsonProperty("from")
    private InstagramUser from;
}
