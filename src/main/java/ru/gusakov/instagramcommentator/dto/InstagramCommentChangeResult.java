package ru.gusakov.instagramcommentator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InstagramCommentChangeResult {
    @JsonProperty("meta")
    private InstagramMeta meta;

}
