package ru.gusakov.instagramcommentator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InstagramCommentList {
    @JsonProperty("data")
    private List<InstagramComment> data = new ArrayList<>();
    @JsonProperty("meta")
    private InstagramMeta meta;

}
