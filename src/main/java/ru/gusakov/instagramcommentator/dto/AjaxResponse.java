package ru.gusakov.instagramcommentator.dto;

import lombok.Data;

@Data
public class AjaxResponse {
    private String message;
    private String status;
    private int code;
    private Object data;
}
