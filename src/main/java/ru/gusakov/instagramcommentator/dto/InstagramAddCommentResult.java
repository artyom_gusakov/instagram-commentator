package ru.gusakov.instagramcommentator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InstagramAddCommentResult {
    @JsonProperty("data")
    private InstagramComment data;

    @JsonProperty("meta")
    private InstagramMeta meta;
}
