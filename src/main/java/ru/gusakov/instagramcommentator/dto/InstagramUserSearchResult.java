package ru.gusakov.instagramcommentator.dto;


import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InstagramUserSearchResult {
    private List<InstagramUser> data = new ArrayList<>();


}

