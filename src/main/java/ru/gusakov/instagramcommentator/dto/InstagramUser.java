package ru.gusakov.instagramcommentator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InstagramUser {
    @JsonProperty("id")
    private String id;
    @JsonProperty("username")
    private String userName;
    @JsonProperty("profile_picture")
    private String profilePicture;
    @JsonProperty("full_name")
    private String fullName;
    @JsonProperty("bio")
    private String bio;
    @JsonProperty("website")
    private String website;
    @JsonProperty("is_business")
    private boolean isBusiness;
}
