package ru.gusakov.instagramcommentator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class InstagramRecentPostResult {
    @JsonProperty(value = "data")
    private List<MediaData> data = new ArrayList<>();

    @Data
    public static class MediaData{
        @JsonProperty("id")
        private String id;
        @JsonProperty("images")
        private Image image;
        @JsonProperty("type")
        private String type;
        @JsonProperty("caption")
        private Caption caption;
    }

    @Data
    public static class Image {
        @JsonProperty("low_resolution")
        private ImageResolution lowResolution;
        @JsonProperty("standard_resolution")
        private ImageResolution standardResolution;
        @JsonProperty("thumbnail")
        private ImageResolution thumbnail;
    }

    @Data
    public static class ImageResolution{
        @JsonProperty("url")
        private String url;
    }

    @Data
    public static class Caption{
        @JsonProperty("text")
        private String text;
    }
}
