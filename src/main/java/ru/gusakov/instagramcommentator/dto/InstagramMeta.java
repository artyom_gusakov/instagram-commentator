package ru.gusakov.instagramcommentator.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class InstagramMeta {
    @JsonProperty("code")
    private int code;
}
