package ru.gusakov.instagramcommentator.model.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(name = "comments", schema = "public")
@Data
@ToString(exclude = {"from", "post"})
public class CommentEntity {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "text", length = 1024)
    private String text;

    @Column(name = "created_time")
    private Timestamp createdTime;

    @Column(name = "sync_date")
    private Timestamp syncDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private AccountEntity from;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "media_id", referencedColumnName = "id")
    private RecentPostEntity post;
}
