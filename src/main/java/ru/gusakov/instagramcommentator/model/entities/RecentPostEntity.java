package ru.gusakov.instagramcommentator.model.entities;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "recent_post", schema = "public")
@Data
@ToString(exclude={"account","comments"})
public class RecentPostEntity {
    @Id
    @Column(name = "id", unique = true)
    private String id;

    @Column(name = "low_resolution")
    private String lowResolution;

    @Column(name = "normal_resolution")
    private String normalResolution;

    @Column(name = "thumbnail")
    private String thumbnail;

    @Column(name="sync_date")
    private Timestamp syncDate;

    @Column(name = "caption")
    private String caption;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id", referencedColumnName = "id")
    private AccountEntity account;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "post")
    private List<CommentEntity> comments = new ArrayList<>();

}
