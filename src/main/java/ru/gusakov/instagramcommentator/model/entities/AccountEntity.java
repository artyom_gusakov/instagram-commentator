package ru.gusakov.instagramcommentator.model.entities;


import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "account", schema = "public")
@Data
@ToString(exclude={"posts", "comments"})
public class AccountEntity {
    @Id
    @Column(name = "id", unique = true)
    private String id;

    @Column(name = "username", nullable = false, unique = true)
    private String userName;

    @Column(name = "profile_picture")
    private String profilePicture;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "bio")
    private String bio;


    @Column(name = "website")
    private String website;

    @Column(name = "is_business")
    private boolean isBusiness;

    @Column(name = "sync_date", nullable = false)
    private Timestamp syncDate;


    /*Используется связь для дальнейшего развития, например в отчетах*/
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "account")
    private List<RecentPostEntity> posts = new ArrayList<>();

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "from")
    private List<CommentEntity> comments = new ArrayList<>();
}
