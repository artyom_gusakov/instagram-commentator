package ru.gusakov.instagramcommentator.model.repositories;


import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gusakov.instagramcommentator.model.entities.AccountEntity;

import java.util.List;

@Repository
public interface AccountRepository extends CrudRepository<AccountEntity, String> {
    List<AccountEntity> findByUserNameStartingWith(String prefix);
    AccountEntity findByUserName(String userName);
}
