package ru.gusakov.instagramcommentator.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gusakov.instagramcommentator.model.entities.RecentPostEntity;

@Repository
public interface RecentPostRepository extends CrudRepository<RecentPostEntity, String> {
}
