package ru.gusakov.instagramcommentator.model.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import ru.gusakov.instagramcommentator.model.entities.CommentEntity;

@Repository
public interface CommentRepository extends CrudRepository<CommentEntity, String> {
}
