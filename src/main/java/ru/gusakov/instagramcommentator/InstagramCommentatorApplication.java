package ru.gusakov.instagramcommentator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class InstagramCommentatorApplication {

	public static void main(String[] args) {
		SpringApplication.run(InstagramCommentatorApplication.class, args);
	}
}
