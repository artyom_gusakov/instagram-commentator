package ru.gusakov.instagramcommentator.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.gusakov.instagramcommentator.dto.InstagramRecentPostResult;
import ru.gusakov.instagramcommentator.dto.InstagramUserSearchResult;
import ru.gusakov.instagramcommentator.exception.InternalServerError;
import ru.gusakov.instagramcommentator.exception.PostNotFoundException;
import ru.gusakov.instagramcommentator.exception.UserNotFoundException;
import ru.gusakov.instagramcommentator.model.entities.AccountEntity;
import ru.gusakov.instagramcommentator.model.entities.RecentPostEntity;
import ru.gusakov.instagramcommentator.service.AccountService;
import ru.gusakov.instagramcommentator.service.InstagramApiService;
import ru.gusakov.instagramcommentator.service.RecentPostService;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/accounts")
public class AccountController {

    @Autowired
    private AccountService accountService;

    @Autowired
    private RecentPostService recentPostService;

    @Autowired
    private InstagramApiService instagramApiService;


    @GetMapping(value = "/user-info", params = {"user-name"})
    public String getUserCard(@RequestParam("user-name") String userName, Model model) {
        InstagramUserSearchResult response;
        try {
            response = instagramApiService.findByUserName(userName, 1);
        } catch (Exception e) {
            throw new InternalServerError(e.getClass().getName().toString() + " " + e.getMessage());
        }
        AccountEntity account = new AccountEntity();
        if (response.getData().size() == 0) {
            throw new UserNotFoundException(userName);
        } else {
            account.setId(response.getData().get(0).getId());
            account.setUserName(response.getData().get(0).getUserName());
            account.setFullName(response.getData().get(0).getFullName());
            account.setProfilePicture(response.getData().get(0).getProfilePicture());
            account.setSyncDate(new Timestamp(new Date().getTime()));
            account.setBio(response.getData().get(0).getBio());
            account.setWebsite(response.getData().get(0).getWebsite());
            model.addAttribute("account", accountService.save(account));
        }
        return "user-card";
    }

    @GetMapping(value = "/user-recent-post", params = {"user-id"})
    @Transactional
    public String getUserRecentPost(@RequestParam("user-id") String id, Model model) {
        InstagramRecentPostResult instPosts;
        try {
            instPosts = instagramApiService.findUserRecentPost(id, 100);
        } catch (IOException e) {
            throw new InternalServerError(e.getClass().getName().toString() + " " + e.getMessage());
        }
        if (instPosts.getData().size() == 0) {
            throw new PostNotFoundException("user with id = " + id + " have not any posts");
        }
        AccountEntity accountEntity = accountService.findOne(id);
        List<RecentPostEntity> posts = new ArrayList<>();
        instPosts.getData().forEach(instPost -> {
            RecentPostEntity post = new RecentPostEntity();
            post.setId(instPost.getId());
            post.setAccount(accountEntity);
            accountEntity.getPosts().add(post);
            post.setLowResolution(instPost.getImage().getLowResolution().getUrl());
            post.setNormalResolution(instPost.getImage().getStandardResolution().getUrl());
            post.setThumbnail(instPost.getImage().getThumbnail().getUrl());
            post.setSyncDate(new Timestamp(new Date().getTime()));
            InstagramRecentPostResult.Caption caption = instPost.getCaption();
            if (caption == null) post.setCaption("");
            else post.setCaption(caption.getText());
            posts.add(post);
        });
        model.addAttribute("posts", recentPostService.save(posts));
        return "user-posts";
    }

}
