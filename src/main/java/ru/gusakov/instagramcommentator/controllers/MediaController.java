package ru.gusakov.instagramcommentator.controllers;

import lombok.extern.log4j.Log4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.gusakov.instagramcommentator.dto.InstagramAddCommentResult;
import ru.gusakov.instagramcommentator.dto.InstagramComment;
import ru.gusakov.instagramcommentator.dto.InstagramCommentChangeResult;
import ru.gusakov.instagramcommentator.dto.InstagramCommentList;
import ru.gusakov.instagramcommentator.exception.InternalServerError;
import ru.gusakov.instagramcommentator.exception.PostNotFoundException;
import ru.gusakov.instagramcommentator.model.entities.AccountEntity;
import ru.gusakov.instagramcommentator.model.entities.CommentEntity;
import ru.gusakov.instagramcommentator.model.entities.RecentPostEntity;
import ru.gusakov.instagramcommentator.service.AccountService;
import ru.gusakov.instagramcommentator.service.CommentService;
import ru.gusakov.instagramcommentator.service.InstagramApiService;
import ru.gusakov.instagramcommentator.service.RecentPostService;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

@Controller
@RequestMapping("/media")
@Log4j
public class MediaController {

    @Autowired
    private InstagramApiService instagramApiService;

    @Autowired
    private RecentPostService recentPostService;

    @Autowired
    private AccountService accountService;

    @Autowired
    private CommentService commentService;

    @GetMapping("/{media-id}/comments")
    @Transactional
    public String getMediaComments(@PathVariable("media-id") String mediaId, Model model) {
        log.info("find comments for media with id = " + mediaId);
        InstagramCommentList instComments;
        try {
            instComments = instagramApiService.findMediaComments(mediaId);
        } catch (IOException e) {
            throw new InternalServerError(e.getClass().getName().toString() + " " + e.getMessage());
        }

        if (instComments.getData().size() == 0) {
            throw new PostNotFoundException("comments for with id = " + mediaId + " not found");
        }
        ArrayList<CommentEntity> comments = new ArrayList<>();
        instComments.getData().forEach(instComment -> {
            CommentEntity comment = new CommentEntity();
            comment.setId(instComment.getId());
            comment.setText(instComment.getText());
            comment.setSyncDate(new Timestamp(new Date().getTime()));
            AccountEntity account = accountService.findOne(instComment.getFrom().getId());
            if (account == null) {
                account = new AccountEntity();
                account.setId(instComment.getFrom().getId());
                account.setUserName(instComment.getFrom().getUserName());
                account.setFullName(instComment.getFrom().getFullName());
                account.setProfilePicture(instComment.getFrom().getProfilePicture());
            }
            comment.setFrom(account);
            account.getComments().add(comment);
            RecentPostEntity post = recentPostService.findById(mediaId);
            comment.setPost(post);
            post.getComments().add(comment);
            comments.add(comment);
        });
        model.addAttribute("comments", commentService.save(comments));
        return "comments";
    }

    @DeleteMapping(value = "/{media-id}/comments/{comment-id}", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public String deleteMediaComments(@PathVariable("media-id") String mediaId,
                                      @PathVariable("comment-id") String commentId) {
        log.info("deleteMediaComments mediaId = " + mediaId);
        log.info("deleteMediaComments commentId = " + commentId);
        InstagramCommentChangeResult delRes;
        try {
            delRes = instagramApiService.deleteMediaComment(mediaId, commentId);
        } catch (IOException e) {
            throw new InternalServerError(e.getClass().getName().toString() + " " + e.getMessage());
        }
        if (delRes.getMeta().getCode() == 200) {
            log.info("Comment with id = " + commentId + " successful deleted from instagram");
            commentService.delete(commentId);
            log.info("Comment with id = " + commentId + " successful deleted from db");
            return "{\"status\":200}";
        }
        return "{\"status\":500}";
    }

    @PostMapping(value = "/{media-id}/comments", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    @Transactional
    public InstagramComment addComment(@PathVariable("media-id") String mediaId, @RequestParam String text) {
        InstagramAddCommentResult addResult;
        try {
            addResult = instagramApiService.addMediaComment(mediaId, text);
        } catch (IOException e) {
            throw new InternalServerError(e.getClass().getName().toString() + " " + e.getMessage());
        }

        if (addResult.getMeta().getCode() == 200) {
            CommentEntity comment = new CommentEntity();
            comment.setId(addResult.getData().getId());
            comment.setText(addResult.getData().getText());
            comment.setSyncDate(new Timestamp(new Date().getTime()));
            AccountEntity account = accountService.findOne(addResult.getData().getFrom().getId());
            if (account == null) {
                account = new AccountEntity();
                account.setId(addResult.getData().getFrom().getId());
                account.setUserName(addResult.getData().getFrom().getUserName());
                account.setFullName(addResult.getData().getFrom().getFullName());
                account.setProfilePicture(addResult.getData().getFrom().getProfilePicture());
            }
            comment.setFrom(account);
            account.getComments().add(comment);
            RecentPostEntity post = recentPostService.findById(mediaId);
            comment.setPost(post);
            post.getComments().add(comment);
            commentService.save(comment);
            return addResult.getData();
        } else {
            throw new InternalServerError(Integer.toString(addResult.getMeta().getCode()));
        }
    }

}
