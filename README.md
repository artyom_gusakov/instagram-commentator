# Instagram commentator #
Приложение для работы с постами в Instagram.

### !Ограничения! ###
С недавних пор, все новые приложения, при регистрации в Instagram, попадают в Sandbox Mode.

>Every new app created on the Instagram Platform starts in Sandbox mode. 
This is a fully functional environment that allows you to test the API before submitting your app for review. 
Sandbox mode is ideal for developers who are new to the Instagram Platform and want to explore the API Platform, 
as well as for teams that need multiple clients for development, staging, and other non-live environments [Подробнее...](https://www.instagram.com/developer/sandbox/)

Поэтому, приложение работает с одним тестовым пользователем [gav.hm](https://www.instagram.com/gav.hm/)

### Используемый стек ###
* SpringBoot Web
* SpringBoot Data Jpa
* SpringBoot Thymeleaf
* SpringBoot DevTools
* Lombok
* okhttp
* Postgresql

### Что реализовано ###
* Поиск пользователя по логину
* Сохранение информации о пользователе в БД
* Получение постов пользователя
* Сохранение постов в БД
* Добавление/Удаление комментариев к посту
* Сохранение комментариев в БД

### Как получить ###
* Собрать Maven'ом
* Взять [отсюда](https://bitbucket.org/artyom_gusakov/instagram-commentator/downloads/instagram-commentator-0.0.1-SNAPSHOT.jar) - это jar с embedded tomcat порт 8080. БД instagram_commentator пользователь postgres пароль root

### Интерфейс ###

##### Main форма #####
![Main форма](https://bitbucket.org/artyom_gusakov/instagram-commentator/downloads/main.PNG)

##### Modal форма #####
![Modal форма](https://bitbucket.org/artyom_gusakov/instagram-commentator/downloads/modal.png)